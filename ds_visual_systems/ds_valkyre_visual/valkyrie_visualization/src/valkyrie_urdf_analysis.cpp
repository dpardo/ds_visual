/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * valkyrie_urdf_analysis.cpp
 *
 *  Created on: Aug 24, 2017
 *      Author: depardo
 */

#include <Eigen/Dense>
#include <ds_visual_base/urdf_info.hpp>
#include <dynamical_systems/systems/valkyrie/ValkyrieDimensions.hpp>

int main ( int argc , char* argv[]) {

  UrdfInfo<valk::valkyrieDimensions> murdf("valkyrie");

  size_t n_joints = murdf.getUrdfNumOfJoints();
  size_t n_links  = murdf.getUrdfNumOfLinks();
  std::vector<std::string> joint_names = murdf.getRobotJointNames();
  std::vector<std::string> link_names = murdf.getRobotLinkNames();


  std::cout << "Valkyrie total joints: " << n_joints << std::endl;
  for (auto a : joint_names) {
      std::cout << a << std::endl;
  }

  std::cout << "The root link : " <<  murdf.rootLinkName() << std::endl;
  std::cout << "Valkyrie total links " << n_links << std::endl;
  for (auto a: link_names) {
      std::cout << a << std::endl;
  }


  murdf.printTree();

  std::vector<std::string> ee_list = {
      "leftFoot",
      "rightFoot",
  };

  return 0;
}
