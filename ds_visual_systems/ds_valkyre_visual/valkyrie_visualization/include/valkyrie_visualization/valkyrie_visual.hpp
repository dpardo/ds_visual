/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * pointFootLeg_visual.hpp
 *
 *  Created on: Jul 21, 2017
 *      Author: depardo
 */

#ifndef _INCLUDE_VALKYRIE_VISUAL_HPP
#define _INCLUDE_VALKYRIE_VISUAL_HPP

#include <Eigen/Dense>

#include <ros/ros.h>
#include <ros/package.h>
#include <dynamical_systems/systems/valkyrie/ValkyrieDimensions.hpp>
#include <ds_visual_base/ds_VisualBase.hpp>

class ValkyrieVisual : public DSVisualBase<valk::valkyrieDimensions> {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef valk::valkyrieDimensions::GeneralizedCoordinates_t GeneralizedCoordinates_t;

  ValkyrieVisual():DSVisualBase("valkyrie"){}
  virtual ~ValkyrieVisual(){}

  virtual void getDefaultConfiguration(GeneralizedCoordinates_t & q) override;
  virtual void updateCustomFrames() override;

};

void ValkyrieVisual::getDefaultConfiguration(GeneralizedCoordinates_t & q) {
  q.setZero();
}

void ValkyrieVisual::updateCustomFrames() {
}

#endif /* _INCLUDE_POINTFOOTLEG_VISUAL_HPP */
