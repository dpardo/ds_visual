cmake_minimum_required(VERSION 2.8.3)
project(valkyrie_visualization)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  ds_visual_base
  ds_systems
  valkyrie_description
)

find_package(Eigen3 REQUIRED)

catkin_package(
   INCLUDE_DIRS include 
#  LIBRARIES 
#  CATKIN_DEPENDS
#  DEPENDS
)

###########
## Build ##
###########

include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
)

## Declare a cpp library

## Declare a cpp executable
add_executable(valkyrie_visual src/valkyrie_visual.cpp)
add_executable(valkyrie_urdf_analysis src/valkyrie_urdf_analysis.cpp)

add_dependencies(valkyrie_visual ds_visual_base_generate_messages_cpp)

target_link_libraries(valkyrie_visual
   ${catkin_LIBRARIES}
)

target_link_libraries(valkyrie_urdf_analysis
 ${catkin_LIBRARIES}
 )


#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_quadrotor_visualization.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
