/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * dsVisualBase.hpp
 *
 *  Created on: Jul 5, 2017
 *      Author: depardo
 */

#ifndef DSVISUALBASE_HPP_
#define DSVISUALBASE_HPP_

#include <memory>
#include <ros/ros.h>
#include <urdf/model.h>
#include <urdf_model/joint.h>
#include <urdf_parser_plugin/parser.h>
#include <kdl_parser/kdl_parser.hpp>
#include <tf/transform_broadcaster.h>
#include <robot_state_publisher/robot_state_publisher.h>
#include <ds_visual_base/RobotConfig.h>
#include <ds_visual_base/urdf_info.hpp>

#include <Eigen/Geometry>

/**
 * @brief: This base class handles the visualization of URDF files corresponding
 * to systems defined in the dynamical_systems framework
 */
template <class DIMENSIONS>
class DSVisualBase {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef typename DIMENSIONS::BaseCoordinates_t BaseCoordinates_t;
  typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
  static const int alpha_index = static_cast<int>(DIMENSIONS::BaseCoordinatesOrder::roll);
  static const int beta_index = static_cast<int>(DIMENSIONS::BaseCoordinatesOrder::pitch);
  static const int gamma_index = static_cast<int>(DIMENSIONS::BaseCoordinatesOrder::yaw);

  /**
   * @brief: Creates a visualization node that reads an URDF file and reads messages with
   * the configuration of the robot
   * @param robot_name : the name of the robot to search and set the corresponding package,
   * urdf file name and rostopic name (see urdf_info class).
   */
  DSVisualBase(const std::string & robot_name);
  virtual ~DSVisualBase(){};

  /**
   * @brief: To be called by the user of this base class
   */
  void initializeVisualizer();

  /**
   * @brief: Launches the visualization loop of the robot. Once this loop has started the
   * node publishes the configuration of the robot so it can be visualized in RViz.
   * @return success loop
   */
  bool visRobotLoop();

  /**
   * @brief: This method may be overloaded in case the user needs to publish custom frames
   */
  virtual void updateCustomFrames() {}
  virtual void publishCustomFrames() {}


private:

  void initializeRobotVariables();
  void getRotMatrixFromRPY(const BaseCoordinates_t & q_b , Eigen::Matrix3d & rot);
  void setModelJointPosition(const GeneralizedCoordinates_t & q);
  void setModelBaseTransformation(const GeneralizedCoordinates_t & q);
  void setModelJointFromMessage(const sensor_msgs::JointState & msg);
  void setModelBaseStateFromMessage(const geometry_msgs::Pose & msg);
  void updateBaseTransform(geometry_msgs::TransformStamped & tr);
  void poseCallback(const ds_visual_base::RobotConfig::ConstPtr & msg);


  /**
   * @brief: To be defined by the user with the default configuration of the robot.
   * @param q
   */
  virtual void getDefaultConfiguration(GeneralizedCoordinates_t & q) =0;

  std::unique_ptr<UrdfInfo<DIMENSIONS>> urdf_info_;
  std::unique_ptr<ros::NodeHandle> dsvis_node_;
  std::unique_ptr<ros::Rate> dsvis_loop_rate_;
  ros::Subscriber dsvis_sub_;

  std::vector<std::string> real_robot_joints;
  KDL::Tree kdl_tree;
  tf::TransformBroadcaster broadcaster;
  geometry_msgs::TransformStamped W_X_B;

  std::map<std::string,double> model_joint_positions;
  Eigen::Vector3d floating_base_origin;
  Eigen::Quaternion<double> floating_base_rotation_Q;

  std::unique_ptr<robot_state_publisher::RobotStatePublisher> dsvis_pub_;

  int first_joint_index = 0;
  int nJoints_ = 0;
  bool floating_base_ = false;
};

template <class DIMENSIONS>
DSVisualBase<DIMENSIONS>::DSVisualBase(const std::string & robot_name):
                          urdf_info_(new UrdfInfo<DIMENSIONS>(robot_name)),
                          dsvis_node_(new ros::NodeHandle),
                          dsvis_loop_rate_(new ros::Rate(200)) {


  real_robot_joints = urdf_info_->getRobotJointNames();

}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::initializeVisualizer() {

  // The user of this class is reponsible for ros init!
  //initialize all values that are robot dependent
  initializeRobotVariables();

  // ToDo: What is that 10? Use Parameter
  dsvis_sub_ = dsvis_node_->subscribe(urdf_info_->robotTopic_, 10, &DSVisualBase<DIMENSIONS>::poseCallback,this);

  kdl_parser::treeFromUrdfModel(urdf_info_->urdf_model_, kdl_tree);
  dsvis_pub_ = std::unique_ptr<robot_state_publisher::RobotStatePublisher>(new robot_state_publisher::RobotStatePublisher(kdl_tree));

  // Initialize the name of the world frames and the default pose
  W_X_B.header.frame_id = "WXB";
  //ToDo: Read This from the properties of the URDF? assume it is constant?
  W_X_B.child_frame_id =  "base_link";

  GeneralizedCoordinates_t q_default;
  getDefaultConfiguration(q_default);
  setModelJointPosition(q_default);
  setModelBaseTransformation(q_default);
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::poseCallback(const ds_visual_base::RobotConfig::ConstPtr & msg) {

  // msg contains the pose twist and joints of the robot
  setModelJointFromMessage(msg->joints);
  setModelBaseStateFromMessage(msg->pose);
}

template <class DIMENSIONS>
bool DSVisualBase<DIMENSIONS>::visRobotLoop() {

  updateBaseTransform(W_X_B);
  W_X_B.header.stamp = ros::Time::now();

  // prepare custom frames
  updateCustomFrames();

  /* publish! */
  broadcaster.sendTransform(W_X_B);
  dsvis_pub_->publishTransforms(model_joint_positions, ros::Time::now(),"");
  dsvis_pub_->publishFixedTransforms("",false);

  publishCustomFrames();

  //Not sure
  dsvis_loop_rate_->sleep();

  return true;
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::initializeRobotVariables() {

  int dof_ = DIMENSIONS::kTotalDof;
  nJoints_ = DIMENSIONS::kTotalJoints;

  if(dof_ == nJoints_){
      first_joint_index = 0;
  } else {
      first_joint_index = 6;
      floating_base_= true;
  }
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::setModelJointPosition(const GeneralizedCoordinates_t & q) {

  for(int i = 0 ; i < nJoints_; ++i) {
    model_joint_positions[real_robot_joints[i]] = q(first_joint_index+i);
  }
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::setModelBaseTransformation(const GeneralizedCoordinates_t & q) {

  // This method updates the position and orientation of the robot

  int base_coordinates_index = static_cast<int>(DIMENSIONS::CoordinatesOrder::qb0);
  int base_position_index    = static_cast<int>(DIMENSIONS::BaseCoordinatesOrder::LX);
  Eigen::Matrix3d rot_I_X_B;

  if(!floating_base_) {
      rot_I_X_B.setIdentity();
      floating_base_origin.setZero();
  }
  else {
      for(int i = 0; i < 3 ; i++) {
          /* from base frame to world frame */
          floating_base_origin[i] = q[base_position_index + i];
      }
      BaseCoordinates_t base_pose = q.segment(base_coordinates_index,first_joint_index);
      getRotMatrixFromRPY(base_pose , rot_I_X_B);
  }

  floating_base_rotation_Q = Eigen::Quaternion<double>(rot_I_X_B);
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::setModelJointFromMessage(const sensor_msgs::JointState &msg) {

  for(int i = 0 ; i < nJoints_; i++){
    // Joint values are set according to the JointState convention
    model_joint_positions[real_robot_joints[i]] = msg.position[i];
  }
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::setModelBaseStateFromMessage(const geometry_msgs::Pose &msg) {

  /* q_base (pos) is in world frame */
  floating_base_origin[0] =  msg.position.x;
  floating_base_origin[1] =  msg.position.y;
  floating_base_origin[2] =  msg.position.z;

  double q_x = msg.orientation.x;
  double q_y = msg.orientation.y;
  double q_z = msg.orientation.z;
  double q_w = msg.orientation.w;

  floating_base_rotation_Q = Eigen::Quaternion<double>(q_w , q_x , q_y , q_z);
}

template <class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::updateBaseTransform(geometry_msgs::TransformStamped & trans_msg) {

  trans_msg.transform.translation.x = floating_base_origin[0];
  trans_msg.transform.translation.y = floating_base_origin[1];
  trans_msg.transform.translation.z = floating_base_origin[2];

  trans_msg.transform.rotation.x = floating_base_rotation_Q.x();
  trans_msg.transform.rotation.y = floating_base_rotation_Q.y();
  trans_msg.transform.rotation.z = floating_base_rotation_Q.z();
  trans_msg.transform.rotation.w = floating_base_rotation_Q.w();
}

template<class DIMENSIONS>
void DSVisualBase<DIMENSIONS>::getRotMatrixFromRPY(const BaseCoordinates_t & base_pose,
                                                   Eigen::Matrix3d & rot) {

  double roll_angle  = base_pose(alpha_index);
  double pitch_angle = base_pose(beta_index);
  double yaw_angle   = base_pose(gamma_index);

  Eigen::AngleAxisd roll_rotation (roll_angle, Eigen::Vector3d::UnitX());
  Eigen::AngleAxisd pitch_rotation(pitch_angle, Eigen::Vector3d::UnitY());
  Eigen::AngleAxisd yaw_rotation  (yaw_angle, Eigen::Vector3d::UnitZ());

  //Rotation matrix from single axis rotations
  rot.setZero();
  rot   =   yaw_rotation * pitch_rotation * roll_rotation;
}

template<class DIMENSIONS>
const int DSVisualBase<DIMENSIONS>::alpha_index;

template<class DIMENSIONS>
const int DSVisualBase<DIMENSIONS>::beta_index;

template<class DIMENSIONS>
const int DSVisualBase<DIMENSIONS>::gamma_index;


#endif /* ROBOTVISUALBASE_HPP_ */
