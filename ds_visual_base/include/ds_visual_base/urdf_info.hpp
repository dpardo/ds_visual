/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * urdf_info.hpp
 *
 *  Created on: Aug 10, 2017
 *      Author: Diego Pardo
 */

#ifndef _URDF_INFO_HPP_
#define _URDF_INFO_HPP_

#include <ros/ros.h>
#include <ros/package.h>
#include <urdf/model.h>
#include <memory>
#include <fstream>
#include <sstream>
#include <dynamical_systems/base/Dimensions.hpp>
template <class DIMENSIONS>
class UrdfInfo {

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  UrdfInfo(const std::string & robot_name):robotName_(robot_name) {

    package_name_ = robotName_ + "_description";
    robotTopic_ = robotName_ +"_config_topic";
    urdf_path_ = ros::package::getPath(package_name_) + "/urdf/" + robotName_ + ".urdf";
    if(!urdf_model_.initFile(urdf_path_)) {
        ROS_ERROR("Invalid URDF File");
        exit(EXIT_FAILURE);
    }
    setRobotJointNames();
  }

  std::vector<std::string> getRobotJointNames() { return robot_joint_names_; }
  std::vector<std::string> getRobotLinkNames() {return robot_link_names_;}
  size_t getUrdfNumOfJoints(){ return kUrdfJoints_;}
  size_t getUrdfNumOfLinks() { return kUrdfLinks_;}
  std::string rootLinkName(){return root_link_name_;}

  std::string robotName_;
  std::string package_name_;
  std::string robotTopic_;
  std::string urdf_path_;
  urdf::Model urdf_model_;

  void printTree() {

    for(auto k : robot_link_names_) {
      std::cout << k << " : " << urdf_model_.links_[k]->getParent()->name << std::endl;
    }
  }

private:

  void setRobotJointNames();

  size_t kUrdfJoints_ = 0;
  size_t kUrdfLinks_ = 0;
  std::vector<std::string> robot_joint_names_;
  std::vector<std::string> robot_link_names_;
  std::string root_link_name_;
  size_t kBranches_ = 0;

};

template <class DIMENSIONS>
void UrdfInfo<DIMENSIONS>::setRobotJointNames() {

    std::vector<std::string> names_from_urdf;
    std::vector<std::string> user_robot_joints;

    // mask to use only the active joints,
    // ToDo: Disconnect this class from dimensions?
    //       and interface between the URDF model and Dimensions?
    for (auto joint = urdf_model_.joints_.begin();joint != urdf_model_.joints_.end(); ++joint) {
        names_from_urdf.push_back(joint->first);
    }

    user_robot_joints = DIMENSIONS::joint_names();

    if(user_robot_joints.size() < 1) {
        robot_joint_names_ = names_from_urdf;
    } else {
        robot_joint_names_ = user_robot_joints;
    }
    kUrdfJoints_ = robot_joint_names_.size();

    root_link_name_ = urdf_model_.root_link_->name;

    // get the name of the links in the active joints
    // ToDo: How to recognize links from frames
    for(auto it = robot_joint_names_.begin(); it != robot_joint_names_.end() ; ++it) {

        std::string parent = urdf_model_.joints_[*it]->parent_link_name;
        std::string child  = urdf_model_.joints_[*it]->child_link_name;

        if(parent!=root_link_name_) {
          auto test_p = find(robot_link_names_.begin(), robot_link_names_.end(),parent);
          if(test_p == robot_link_names_.end())
            robot_link_names_.push_back(parent);
        }
        if(child!=root_link_name_) {
          auto test_c = find(robot_link_names_.begin(), robot_link_names_.end(),child);
          if(test_c == robot_link_names_.end())
            robot_link_names_.push_back(child);
        }
    }

    // number of main branches of the robot
    kBranches_= urdf_model_.links_[root_link_name_]->child_links.size();

    kUrdfLinks_ = robot_link_names_.size();

}

#endif /* _URDF_INFO_HPP_ */
