/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/
/*
 * RobotConfigVis
 *
 *  Created on: August 10, 2017
 *      Author: depardo
 */

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <Eigen/Dense>
#include <cmath>

#include <ds_visual_base/RobotConfig.h>
#include <ds_visual_base/urdf_info.hpp>
#include <dynamical_systems/tools/LRConversions.hpp>
#include <dynamical_systems/tools/LeggedRobotTrajectory.hpp>

template <class DIMENSIONS>
class RobotConfigVis : public LRConversions<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW


  typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
	typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;
	typedef typename DIMENSIONS::EELambda_t EELambda_t;
	typedef typename DIMENSIONS::EELambda_array_t EELambda_array_t;
	typedef typename DIMENSIONS::GeneralizedCoordinates_t GeneralizedCoordinates_t;
	typedef typename DIMENSIONS::BaseCoordinates_t BaseCoordinates_t;

	RobotConfigVis(const std::string & robot_name);
	virtual ~RobotConfigVis(){};

	void visualize_pose(const GeneralizedCoordinates_t & big_q);
  void visualize_pose(const state_vector_t & x);


	void PlayTrajectory(const state_vector_array_t & x_solution);
	void PlayTrajectory(const state_vector_array_t & x_traj, const EELambda_array_t & cf_traj);
	void PlayLRTrajectory(const LeggedRobotTrajectory<DIMENSIONS> & hyqlrt);
	void sleep();

	// ToDo:
	//  void getRealContactForce(const GeneralizedCoordinates_t & big_q, const  EELambda_t & original , EELambda_t & cf);
	//  void DrawSupportPolygon(const HyQState & hyq_state);
	//  void DrawPolygon(const Eigen::MatrixXd & p_points);
	//  void DrawZmp(const Eigen::Vector2d & zmp);
  //  void DrawVector(const Eigen::Vector3d & vec, const std::string & frame, int client_id, double scale, int color = 1);
  //  void DrawFeetPosition(const GeneralizedCoordinates_t & big_q);
  //  void DrawGravity(const GeneralizedCoordinates_t & big_q);
  //  void DrawContactForce(const EELambda_t & lamda);
  //  void DrawImpact(const EELambda_t & impact);
  //  void DrawForce(const EELambda_t & force,const int & client_id,
  //                 const int & scale = 200, const int & color = 1);


private:

	void getMessageFromRobotPose(const GeneralizedCoordinates_t & big_q,
	                             ds_visual_base::RobotConfig & msg);
	void SetJointPositionMessage(const GeneralizedCoordinates_t & big_q,
	                             sensor_msgs::JointState & msg);
	void SetBasePoseMessage(const GeneralizedCoordinates_t &big_q,
	                        geometry_msgs::Pose & msg);


  std::unique_ptr<ros::NodeHandle> node_;
	std::unique_ptr<ros::Publisher> publisher_;
	std::unique_ptr<ros::Rate> rate_;
	std::string topic_name_;
	GeneralizedCoordinates_t robot_pose_;
	std::vector<std::string> robot_joint_names_;
	UrdfInfo<DIMENSIONS> urdf_info_;

// TODO: import this from hyq implementation
//	ros::Publisher mark_array_pub;
//	visualization_msgs::MarkerArray additional_markers;

//  ros::Publisher feet_position_pub;
//  ros::Publisher gravity_pub;
//	ros::Publisher polygonSup_pub;
//	ros::Publisher zmp_pub;

};

template<class DIMENSIONS>
RobotConfigVis<DIMENSIONS>::RobotConfigVis(const std::string & robot_name): urdf_info_(robot_name) {


  robot_joint_names_ = urdf_info_.getRobotJointNames();
  node_ = std::unique_ptr<ros::NodeHandle>(new ros::NodeHandle);
  rate_ = std::unique_ptr<ros::Rate>(new ros::Rate(200));
  publisher_ = std::unique_ptr<ros::Publisher>(new ros::Publisher(node_->advertise<ds_visual_base::RobotConfig>(urdf_info_.robotTopic_,1)));
  sleep();
//  feet_position_pub = urdf_node_->advertise<visualization_msgs::MarkerArray>("feet_vis_marker_array",1);
//  gravity_pub       = urdf_node_->advertise<visualization_msgs::Marker>("gravity_marker",1);
//  mark_array_pub    = urdf_node_->advertise<visualization_msgs::MarkerArray>("hyq_marker_array",1);
//  polygonSup_pub    = urdf_node_->advertise<geometry_msgs::PolygonStamped>("support_polygon",1);
//  zmp_pub           = urdf_node_->advertise<geometry_msgs::PointStamped>("zmp",1);

}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::visualize_pose(const GeneralizedCoordinates_t & big_q) {

  ds_visual_base::RobotConfig config_message;
  getMessageFromRobotPose(big_q , config_message);
  while(publisher_->getNumSubscribers() < 1);
  publisher_->publish(config_message);
  ros::spinOnce();
  sleep();
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::visualize_pose(const state_vector_t & x) {

  GeneralizedCoordinates_t q,qd;
  this->GeneralizedCoordinatesFromStateVector(x,q,qd);
  visualize_pose(q);
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::getMessageFromRobotPose(const GeneralizedCoordinates_t & big_q ,
    ds_visual_base::RobotConfig & msg) {

  SetJointPositionMessage(big_q,msg.joints);
  SetBasePoseMessage(big_q,msg.pose);
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::SetJointPositionMessage(const GeneralizedCoordinates_t & big_q ,
    sensor_msgs::JointState &msg) {

  for(int i = 0 ; i < this->kTotalJoints ; i++){
    msg.name.push_back(robot_joint_names_[i]);
    msg.position.push_back(big_q[i+this->kBaseDof]);
  }
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::SetBasePoseMessage(const GeneralizedCoordinates_t & big_q ,
    geometry_msgs::Pose &msg) {

  BaseCoordinates_t qb;

  this->GetFloatingBaseCoordinates(big_q,qb);

  msg.position.x = qb(3);
  msg.position.y = qb(4);
  msg.position.z = qb(5);

  Eigen::Affine3d A_wb;
  this->GetWBTransform(qb , A_wb);

  Eigen::Quaternion<double> robot_orientation_Q(A_wb.rotation());

  msg.orientation.x = robot_orientation_Q.x();
  msg.orientation.y = robot_orientation_Q.y();
  msg.orientation.z = robot_orientation_Q.z();
  msg.orientation.w = robot_orientation_Q.w();
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::PlayTrajectory(const state_vector_array_t & x_solution) {

  GeneralizedCoordinates_t big_q,qd;

  for(auto const x : x_solution) {
    this->GeneralizedCoordinatesFromStateVector(x,big_q,qd);
    visualize_pose(big_q);
  }
}

template<class DIMENSIONS>
void RobotConfigVis<DIMENSIONS>::sleep(){
  rate_->sleep();
}
